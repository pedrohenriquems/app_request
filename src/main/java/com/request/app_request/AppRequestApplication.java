package com.request.app_request;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppRequestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppRequestApplication.class, args);
	}
}
